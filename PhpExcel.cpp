/*PhpExcel.cpp*/
#include "PhpExcel.h"
#include "zend_exceptions.h"
/*--------------------------*/
#include "libxl.h"
#include "strings.h" //strcasecmp

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>


using namespace libxl;


static zend_class_entry *PhpExcel_easy_ce = NULL;
static zend_object_handlers PhpExcel_easy_handlers;

ZEND_DECLARE_MODULE_GLOBALS(PhpExcel);


//interim
typedef struct _PhpExcel_easy_object {
	libxl::Book *handle;
	libxl::Sheet *cursheet;
	zend_object std;
}PhpExcel_easy_object;

static zend_object* PhpExcel_easy_to_zend_object(PhpExcel_easy_object *objval) {
	return ((zend_object*)(objval + 1)) - 1;
}

static PhpExcel_easy_object* PhpExcel_easy_from_zend_object(zend_object *objval) {
	return ((PhpExcel_easy_object*)(objval + 1)) - 1;
}

/*function start*/
PHP_METHOD(PhpExcel, LoadFile) {
	/*
	if (zend_parse_parameters_none_throw() == FAILURE) {
		return;
	}
	*/
	char *file_name = NULL;
	size_t name_len;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "s", &file_name, &name_len) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	RETURN_BOOL(objval->handle->load(file_name));
}

PHP_METHOD(PhpExcel, SheetCount) {
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	RETURN_LONG(objval->handle->sheetCount());
}

PHP_METHOD(PhpExcel, SheetSwitch) {
	long sheet_index;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "l", &sheet_index) == FAILURE) {
		RETURN_FALSE
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	long sheet_count = objval->handle->sheetCount();
	libxl::Sheet *pSheet = objval->handle->getSheet(sheet_index<0?0:sheet_index>sheet_count?sheet_count:sheet_index);
	if(NULL!=pSheet){
		objval->cursheet = pSheet;
		RETURN_TRUE;
	}
	RETURN_FALSE;
}

PHP_METHOD(PhpExcel, AddSheetAndSwitchTo) {
	char *sheet_name = NULL;
	long sheet_name_len;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "s", &sheet_name, &sheet_name_len) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	objval->cursheet = objval->handle->addSheet(sheet_name);
	RETURN_BOOL(objval->cursheet!=NULL);
}

PHP_METHOD(PhpExcel, GetSheetNameByIndex) {
	long sheet_index;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "l", &sheet_index) == FAILURE) {
		RETURN_STRING("");
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	long sheet_count = objval->handle->sheetCount();
	libxl::Sheet *pSheet = objval->handle->getSheet(sheet_index<0?0:sheet_index>sheet_count?sheet_count:sheet_index);
	if(NULL!=pSheet){
		const char *sheet_name = pSheet->name();
		RETURN_STRINGL(sheet_name, strlen(sheet_name));
	}
	RETURN_STRING("");
}

PHP_METHOD(PhpExcel, SetSheetNameByIndex) {
	long sheet_index;
	char *sheet_name = NULL;
	size_t name_len;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ls", &sheet_index, &sheet_name, &name_len) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	long sheet_count = objval->handle->sheetCount();
	libxl::Sheet *pSheet = objval->handle->getSheet(sheet_index<0?0:sheet_index>sheet_count?sheet_count:sheet_index);
	if(NULL!=pSheet){
		pSheet->setName(sheet_name);
		RETURN_TRUE
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, GetTotalRow) {
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_LONG(objval->cursheet->lastRow());
	}
	RETURN_LONG(0)
}

PHP_METHOD(PhpExcel, GetTotalCol) {
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_LONG(objval->cursheet->lastCol());
	}
	RETURN_LONG(0)
}

PHP_METHOD(PhpExcel, GetCellType) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_LONG(5);//TYPE_ERROR
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_LONG(objval->cursheet->cellType(row, col));
	}
	RETURN_LONG(5);//TYPE_ERROR
}

PHP_METHOD(PhpExcel, ReadNum) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_DOUBLE(0.0)
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		//const double val = objval->cursheet->readNum(row, col);
		//RETURN_DOUBLE(val);
		RETURN_DOUBLE(objval->cursheet->readNum(row, col));
	}
	RETURN_DOUBLE(0.0);
	
}

PHP_METHOD(PhpExcel, ReadStr) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_STRING("");
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		const char *str = objval->cursheet->readStr(row, col);
		RETURN_STRINGL(str, strlen(str));
	}
	RETURN_STRING("");
}

PHP_METHOD(PhpExcel, WriteNum) {
	long row, col;
	double num;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "lld", &row, &col, &num) == FAILURE) {
		RETURN_FALSE
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->writeNum(row, col, num));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, WriteStr) {
	long row, col,formula_len;
	char *str = NULL;
	long  is_bold=0, font_color=8;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "llsll", &row, &col, &str,&formula_len, &is_bold,&font_color) == FAILURE) {
		RETURN_FALSE
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
        if (is_bold==0 && font_color==8 ){
            RETURN_BOOL(objval->cursheet->writeStr(row, col, str));
        }else{
            Book* book = objval->handle;
            if(book){
                Font* font = book->addFont();
                font->setColor(Color(font_color));
                font->setBold(is_bold);
                Format* boldFormat = book->addFormat();
                boldFormat->setFont(font);
                RETURN_BOOL(objval->cursheet->writeStr(row, col, str,boldFormat));
            }
        }
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, AddPicture) {
	long row,col,pic_path_len;
    char *pic_path = NULL;
    int pic_id ;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "lls", &row,&col,&pic_path,&pic_path_len) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if (objval->cursheet != NULL) {
        Book* book = objval->handle;
		if(pic_path &&  book){
            pic_id = book->addPicture(pic_path);
            objval->cursheet->setPicture(row, col, pic_id) ;
		}
	}
}

PHP_METHOD(PhpExcel, IsFormula) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->isFormula(row, col));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, ReadFormula) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_STRING("")
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		const char *str = objval->cursheet->readFormula(row, col);
		RETURN_STRINGL(str, strlen(str));
	}
	RETURN_STRING("")
}

PHP_METHOD(PhpExcel, WriteFormula) {
	long row, col,formula_len; 
	char *formula = NULL;
	long  is_bold=0;
	long font_color=8 ;
	
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "llsll", &row, &col, &formula,&formula_len, &is_bold, &font_color) == FAILURE) {
		RETURN_FALSE
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
        if (is_bold==0 && font_color==8 ){
             RETURN_BOOL(objval->cursheet->writeFormula(row, col, formula));
        }else{
            Book* book = objval->handle;
            if(book){
                Font* font = book->addFont();
                font->setColor(Color(font_color));
                font->setBold(is_bold);
                Format* boldFormat = book->addFormat();
                boldFormat->setFont(font);
                RETURN_BOOL(objval->cursheet->writeFormula(row, col, formula, boldFormat));
            }
        }
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, IsDate) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->isDate(row, col));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, SetMerge) {
	long rowFirst, colFirst, rowLast, colLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "llll", &rowFirst, &colFirst, &rowLast, &colLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->setMerge(rowFirst, rowLast, colFirst, colLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, DelMerge) {
	long row, col;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &row, &col) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->delMerge(row, col));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, InsertRow) {
	long rowFirst, rowLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &rowFirst, &rowLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->insertRow(rowFirst, rowLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, DelRow) {
	long rowFirst, rowLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &rowFirst, &rowLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->removeRow(rowFirst, rowLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, InsertCol) {
	long colFirst, colLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &colFirst, &colLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->insertCol(colFirst, colLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, DelCol) {
	long colFirst, colLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &colFirst, &colLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->removeCol(colFirst, colLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, CopyCell) {
	long rowSrc, colSrc, rowDst, colDst;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "llll", &rowSrc, &colSrc, &rowDst, &colDst) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->copyCell(rowSrc, colSrc, rowDst, colDst));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, ClearCell) {
	long rowFirst, colFirst, rowLast, colLast;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "llll", &rowFirst, &colFirst, &rowLast, &colLast) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->clear(rowFirst, rowLast, colFirst, colLast));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, SetRowHeigh) {
	long row;
	double heigh;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "ld", &row, &heigh) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->setRow(row, heigh));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, SetColWidth) {
	long colFirst, colLast;
	double width;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "lld", &colFirst, &colLast, &width) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->setCol(colFirst, colLast, width));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, RowHide) {
	long row;
	bool hide;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "lb", &row, &hide) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->setRowHidden(row, hide));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, ColHide) {
	long col;
	bool hide;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "lb", &col, &hide) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if(objval->cursheet!=NULL){
		RETURN_BOOL(objval->cursheet->setColHidden(col, hide));
	}
	RETURN_FALSE
}

PHP_METHOD(PhpExcel, SaveToFile) {
	char *sheet_name = NULL;
	long sheet_name_len;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "s", &sheet_name, &sheet_name_len) == FAILURE) {
		RETURN_FALSE;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	RETURN_BOOL(objval->handle->save(sheet_name));
}

static PHP_METHOD(PhpExcel, __construct) {
	char* name = NULL;
	size_t name_len;
	if (zend_parse_parameters_throw(ZEND_NUM_ARGS() TSRMLS_CC, "s", &name, &name_len) == FAILURE){
		return;
	}
	PhpExcel_easy_object *objval = PhpExcel_easy_from_zend_object(Z_OBJ_P(getThis()));
	if( strcasecmp("xlsx", name) == 0) {
		objval->handle = xlCreateXMLBook();
	}else{
		objval->handle = xlCreateBook();
	}
	if(NULL!=objval->handle){
		objval->handle->setKey("lucas.D", "linux-2927280408c8e1006bb26867a5t2u3h5");
		objval->cursheet = NULL;
	}
}

//apis 
static zend_function_entry PhpExcel_easy_methods[] = {
	PHP_ME(PhpExcel, __construct, NULL, ZEND_ACC_CTOR | ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, LoadFile, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SheetCount, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SheetSwitch, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, AddSheetAndSwitchTo, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, GetSheetNameByIndex, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SetSheetNameByIndex, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, GetTotalRow, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, GetTotalCol, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, GetCellType, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, ReadNum, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, ReadStr, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, WriteNum, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, WriteStr, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, AddPicture, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, IsFormula, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, ReadFormula, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, WriteFormula, NULL, ZEND_ACC_PUBLIC)
	//PHP_ME(PhpExcel, IsDate, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SetMerge, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, DelMerge, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, InsertRow, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, DelRow, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, InsertCol, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, DelCol, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, CopyCell, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, ClearCell, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SetRowHeigh, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SetColWidth, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, RowHide, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, ColHide, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(PhpExcel, SaveToFile, NULL, ZEND_ACC_PUBLIC)
	PHP_FE_END
};

//ctor
static zend_object* PhpExcel_easy_ctor(zend_class_entry *ce) {
	PhpExcel_easy_object *objval = (PhpExcel_easy_object*)ecalloc(1, sizeof(PhpExcel_easy_object) + zend_object_properties_size(ce));
	//objval->handle = new Person(0,"");
	objval->handle = NULL;
	zend_object* ret = PhpExcel_easy_to_zend_object(objval);
	zend_object_std_init(ret, ce);
	object_properties_init(ret, ce);
	ret->handlers = &PhpExcel_easy_handlers;
	return ret;
}


//clone	--not registered
static zend_object* PhpExcel_easy_clone(zval *srcval) {
	zend_object *zsrc = Z_OBJ_P(srcval);
	zend_object *zdst = PhpExcel_easy_ctor(zsrc->ce);
	zend_objects_clone_members(zdst, zsrc);
	return zdst;
	/*need deep copy*/
	PhpExcel_easy_object *src = PhpExcel_easy_from_zend_object(zsrc);
	PhpExcel_easy_object *dst = PhpExcel_easy_from_zend_object(zdst);
	*dst->handle = *src->handle;   
	return zdst;
}

//delete
static void PhpExcel_easy_free(zend_object *zobj) {
	PhpExcel_easy_object *obj = PhpExcel_easy_from_zend_object(zobj);
	if (obj->handle!=NULL){
		obj->cursheet = NULL;
		obj->handle->release();
	}
	zend_object_std_dtor(zobj);
}


/*ready function*/
PHP_INI_BEGIN()
	//no globals
	//STD_PHP_INI_ENTRY("tutorial.default_url", "", PHP_INI_ALL,OnUpdateString, default_url, zend_tutorial_globals, tutorial_globals)
PHP_INI_END()


static PHP_MINIT_FUNCTION(PhpExcel) {
	zend_class_entry ce;
	INIT_CLASS_ENTRY(ce, "PhpExcel", PhpExcel_easy_methods);
	PhpExcel_easy_ce = zend_register_internal_class(&ce);
	PhpExcel_easy_ce->create_object = PhpExcel_easy_ctor;
	memcpy(&PhpExcel_easy_handlers, zend_get_std_object_handlers(), sizeof(zend_object_handlers));
	PhpExcel_easy_handlers.offset = XtOffsetOf(PhpExcel_easy_object, std);
	//PhpExcel_easy_handlers.clone_obj = PhpExcel_easy_clone;	//not supported
	PhpExcel_easy_handlers.free_obj = PhpExcel_easy_free;
	REGISTER_INI_ENTRIES();
	return SUCCESS;
}
static PHP_MSHUTDOWN_FUNCTION(PhpExcel) {
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}

static PHP_GINIT_FUNCTION(PhpExcel) {
#if defined(COMPILE_DL_ASTKIT) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif
	//init global??
}

zend_module_entry PhpExcel_module_entry = {
	STANDARD_MODULE_HEADER,
	"PhpExcel",
	NULL, /* functions */
	PHP_MINIT(PhpExcel),
	PHP_MSHUTDOWN(PhpExcel),
	NULL,/*print*/
	NULL,/*rshutdown*/
	NULL,/*minfo*/
	NO_VERSION_YET,
	PHP_MODULE_GLOBALS(PhpExcel),
	PHP_GINIT(PhpExcel),
	NULL,/*gshutdown*/
	NULL,/*rpostshutdown*/
	STANDARD_MODULE_PROPERTIES_EX


};


#ifdef COMPILE_DL_PHPEXCEL
	ZEND_GET_MODULE(PhpExcel)
#endif