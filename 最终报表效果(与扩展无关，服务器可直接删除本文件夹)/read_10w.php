<?php

// 读取10w数据测试

//读取Excel
$obj_excel = new PhpExcel("xlsx");
$obj_excel->LoadFile(date('Y-m-d').'.xlsx');
$obj_excel->SheetSwitch(0);  //  切换到第一个sheet（序号从0开始）表准备读取数据
$row = $obj_excel->GetTotalRow();
$col = $obj_excel->GetTotalCol();
echo "读取数据的行数、列数：{$row}、{$col}\n";

$temp_row='';
$temp_col='';
for($r = 0; $r < $row; $r++){
    for($c = 0; $c < 10; $c++){
        $temp_col=$obj_excel->ReadStr($r, $c);
        $temp_row.= $temp_col.' | ';
    }
    $temp_row.="\n";

}
file_put_contents("./readResult.txt",$temp_row,FILE_APPEND) ;

// 测试  php  ./read_10w.php  即可



