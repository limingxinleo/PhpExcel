<?php

// 生成10万行数据测试

    $obj_excel = new PhpExcel("xlsx");
    $obj_excel->AddSheetAndSwitchTo("tsheet");
    $obj_excel->AddPicture(0, 11,'./hyperf.jpg');   //  第一行添加一张图片测试
    for($r = 0; $r < 100000; $r++){
        for($c = 0; $c < 10; $c++){
            $obj_excel->WriteStr($r, $c, "({$r},{$c}) 测试用例数据",0,8);
        }
    }

    $obj_excel->SaveToFile(date('Y-m-d').'.xlsx');


// 测试  php  ./create_10w.php  即可
