###   PhpExcel支持Excel格式 
	.xls & .xlsx
	特别说明：c++扩展，速度超快，本人比较过类似库，自认为本库是使用最简单的，同时功能又是非常强大的！内核是调用了老外的库，我只是把它封装为php的调用接口而已，本人的角色仅仅是一个代码搬运工  

###    构建（以CnentOs为例）  
    git clone https://gitee.com/daitougege/PhpExcel.git
    
    cd PhpExcel
    
    /usr/local/php/bin/phpize     //   如果系统有多个版本的php，必须指定对应的php路径
  
    ./configure   --with-php-config=/usr/local/php/bin/php-config   // 带上正确配置文件  
  
    make  &&   make install  
  

    //  修改php.ini配置文件增加扩展
	vim /usr/local/php/etc/php.ini
	//添加以下扩展
    extension=PhpExcel.so
    
	// 重启php-fpm，
    service  php-fpm  restart   //  可以通过service  --status-all 或者  systemctl  status 查看php在centos或者ubutu注册的服务名称，ubutu注册的名称可能是 php-fpm7.2
    //检查扩展是否增加
	/usr/local/php/bin/php -m

###  生成Excel数据
>   文件名：create_10w.php    ; 测试  php  ./create_10w.php  即可
```php
        <?php

        // 生成10万行数据测试

            $obj_excel = new PhpExcel("xlsx"); //  如果是框架里面都会有命名空间，使用方法 -->  new \PhpExcel("xlsx");
            $obj_excel->AddSheetAndSwitchTo("tsheet");
            $obj_excel->AddPicture(0, 11,'./hyperf.jpg');   //  第一行添加一张图片测试
            for($r = 0; $r < 100000; $r++){
                for($c = 0; $c < 10; $c++){
                    $obj_excel->WriteStr($r, $c, "({$r},{$c}) 测试用例数据",0,8);
                }
            }

            $obj_excel->SaveToFile(date('Y-m-d').'.xlsx');  //  以当天日期命名

```

###  读取Excel
>   文件名：read_10w.php ；  测试  php  ./read_10w.php  即可
```php
        <?php

        // 读取10w数据测试

        $obj_excel = new PhpExcel("xlsx");   //  如果是框架里面都会有命名空间，使用方法 -->  new \PhpExcel("xlsx");
        $obj_excel->LoadFile(date('Y-m-d').'.xlsx');  //  读取刚才生成的excel文件
        $obj_excel->SheetSwitch(0);  //  切换到第一个sheet（序号从0开始）表准备读取数据
        $row = $obj_excel->GetTotalRow();
        $col = $obj_excel->GetTotalCol();
        echo "读取数据的行数、列数：{$row}、{$col}\n";

        $temp_row='';
        $temp_col='';
        for($r = 0; $r < $row; $r++){
            for ($c = 0; $c < $col; $c++) {
                // 首先获取单元格类型（参见后面单元格类型，）
                $cellType=$obj_excel->GetCellType($r, $c);
                //  我们读取数字或者文本
                if($cellType ==1  ||  $cellType==2){
                    $temp_col = $obj_excel->ReadStr($r, $c);   //  千万不要在未判断单元格类型的时候直接读，遇到空白、空值时，本函数会报错
                }else{
                    $temp_col='' ;
                }
                $temp_row .= $temp_col . ' | ';   //  将所有列拼接成一行
            }
            $temp_row.="\n";

        }
        file_put_contents("./readResult.txt",$temp_row,FILE_APPEND) ;  // 保存在txt，验证一下数据

```
    
###  所有Api函数列表
```constants
    LoadFile             		  //加载一个文件                     		参数：文件名                                 													返回：bool
    SheetCount           		  //返回sheet总数                    		参数：无                                     													返回：int
    SheetSwitch          		  //切换sheet                        		参数：sheet下标                              													返回：bool
    AddSheetAndSwitchTo  		  //添加一张新sheet，并切换到该sheet 		参数：sheet名                                													返回：bool
    GetSheetNameByIndex  		  //跟据序号获取sheet名，序号从0开始 		参数：sheet下标                              													返回：str
    SetSheetNameByIndex  		  //跟据序号设置sheet名，序号从0开始 		参数：sheet下标，sheet新名                   													返回：bool
    GetTotalRow          		  //获取当前sheet的总行数            		参数：无                                     													返回：int
    GetTotalCol          		  //获取当前sheet总列数              		参数：无                                     													返回：int
    GetCellType          		  //获取单元格数据类型               		参数：row, col                               													返回：int（CellType见附表相关的值）
    ReadNum              		  //读取数字                         		参数：row, col                               													返回：int
    ReadStr              		  //读取字符串,首先需要判断单元格类型       参数：row, col                               													返回：str
    WriteNum             		  //写入数字                         		参数：row, col, num                          													返回：bool
    WriteStr             		  //写入字符串                       		参数：row, col, str,is_bold(是否加粗1|0),font_color（见附表）         返回：bool
    IsFormula            		  //该单元格是否是公式               		参数：row, col                               													返回：bool
    ReadFormula          		  //读取公式                         		参数：row, col                               													返回：str
    WriteFormula         		  //写入公式                         		参数：row, col, str ,is_bold(是否加粗1|0),font_color（见附表）        返回：bool
    SaveToFile           		  //保存到文件                       		参数：s文件名                                													返回：bool
    SetMerge             		  //合并单元格                       		参数：rowFirst, colFirst, rowLast, colLast   													返回：bool
    DelMerge             		  //取消已合并的单元格               		参数：row, col                               													返回：bool
    InsertRow            		  //插入行                           		参数：rowFirst, rowLast                      													返回：bool
    DelRow               		  //删除行                           		参数：rowFirst, rowLast                      													返回：bool
    InsertCol            		  //插入列                           		参数：colFirst, colLast                      													返回：bool
    DelCol               		  //删除列                           		参数：colFirst, colLast                      													返回：bool
    CopyCell             		  //复制单元格                       		参数：rowSrc, colSrc, rowDst, colDst         													返回：bool
    ClearCell            		  //清空单元格                       		参数：rowFirst, colFirst, rowLast, colLast   													返回：bool
    SetRowHeigh          		  //设置行高                         		参数：row, heigh                             													返回：bool
    SetColWidth          		  //设置列宽                         		参数：colFirst, colLast, width               													返回：bool
    RowHide              		  //隐藏行                           		参数：row, hide->BOOL                        													返回：bool
    ColHide              		  //隐藏列                           		参数：col, hide->BOOL                        													返回：bool
    AddPicture                    //单元格添加图片                          参数：row, col, pic_path                       													返回：无返回值
```
    
###   CellType 单元格数据类型
    0			CELLTYPE_EMPTY      空值（类似null）
    1			CELLTYPE_NUMBER     数字
    2			CELLTYPE_STRING     文本字符串
    3			CELLTYPE_BOOLEAN    布尔型
    4			CELLTYPE_BLANK      空白（类似敲了一个空格）
    5			CELLTYPE_ERROR      错误

###  color 常量对照表
    8	  		COLOR_BLACK
    9	  		COLOR_WHITE
    10			COLOR_RED
    11			COLOR_BRIGHTGREEN
    12			COLOR_BLUE
    13			COLOR_YELLOW
    14			COLOR_PINK
    15			COLOR_TURQUOISE
    16			COLOR_DARKRED
    17			COLOR_GREEN
    18			COLOR_DARKBLUE
    19			COLOR_DARKYELLOW
    20			COLOR_VIOLET
    21			COLOR_TEAL
    22			COLOR_GRAY25
    23			COLOR_GRAY50
    24			COLOR_PERIWINKLE_CF
    25			COLOR_PLUM_CF
    26			COLOR_IVORY_CF
    27			COLOR_LIGHTTURQUOISE_CF
    28			COLOR_DARKPURPLE_CF
    29			COLOR_CORAL_CF
    30			COLOR_OCEANBLUE_CF
    31			COLOR_ICEBLUE_CF
    32			COLOR_DARKBLUE_CL
    33			COLOR_PINK_CL
    34			COLOR_YELLOW_CL
    35			COLOR_TURQUOISE_CL
    36			COLOR_VIOLET_CL
    37			COLOR_DARKRED_CL
    38			COLOR_TEAL_CL
    39			COLOR_BLUE_CL
    40			COLOR_SKYBLUE
    41			COLOR_LIGHTTURQUOISE
    42			COLOR_LIGHTGREEN
    43			COLOR_LIGHTYELLOW
    44			COLOR_PALEBLUE
    45			COLOR_ROSE
    46			COLOR_LAVENDER
    47			COLOR_TAN
    48			COLOR_LIGHTBLUE
    49			COLOR_AQUA
    50			COLOR_LIME
    51			COLOR_GOLD
    52			COLOR_LIGHTORANGE
    53			COLOR_ORANGE
    54			COLOR_BLUEGRAY
    55			COLOR_GRAY40
    56			COLOR_DARKTEAL
    57			COLOR_SEAGREEN
    58			COLOR_DARKGREEN
    59			COLOR_OLIVEGREEN
    60			COLOR_BROWN
    61			COLOR_PLUM
    62			COLOR_INDIGO
    63			COLOR_GRAY80

###  测试结果..
    2核+4G内存 百度云服务器
    10万行 10列数据写入Excel，10-15秒
    10万行 10列数据读取10-15秒
    平均计算结果：处理1万行10列数据在1秒左右